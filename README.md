Background:

Este reto es obligatorio para continuar el proceso de selección para el puesto de desarrollador web dentro de geosatelital, te aconsejo lo hagas en un ambiente tranquilo ya que te puede tomar un mediano tiempo.
Son 4 retos de backend (php, ruby o python) y 2 retos de frontend (js/html/css).
Para la entrevista final solo tomaremos en cuenta a los que hayan terminado los retos.

Adicionalmente agregamos unas preguntas en el archivo cuestionario.md que debes responder.

Suerte!

Instrucciones:
* 1) forkear/clonar este repositorio
* 2) resolver los retos, debes elegir al menos 1 de los 3 (php,ruby,python), si deseas hacer tu implementacion en JS bienvenido...
* 3) en el archivo index.html consumir los ultimos 10 tweets de https://twitter.com/ThePracticalDev , y al clickar en uno de los tweets. pintar el hilo en un modal (solo 1 nivel)
* 4) responder las preguntas del archivo cuestionario.md
* 5) pushear todo y enviar al mail plataforma@geosatelital.com tu repositorio publico de la solución
