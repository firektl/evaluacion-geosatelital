/*  Evaluación realizada por Fiorela Torres
 */
$(function(){

	moment.locale('en');
	user_get = 'ThePracticalDev';
	count = 10;

	$.getJSON('twitter-proxy.php?url='+encodeURIComponent('statuses/user_timeline.json?screen_name='+user_get+'&count='+count+'&tweet_mode=extended&locale=es&lang=es&iso_language_code=es'), function(data){

		for(var i in data) {

			console.log (data[i]);

			htmlRT = ""; 
			twit = data[i];
			usuario_retwitt = "@" + twit.user.screen_name;
			id_str = twit.id_str;

			if (twit.retweeted_status){
				twit = twit.retweeted_status;
				htmlRT = "<div class='w-100 text-right text-muted p-1'><small><i class='fas fa-retweet'></i> Retwitt "+ usuario_retwitt  +"</small></div>";
			}  

			img_perfil = twit.user.profile_image_url;
			usuario = "@" + twit.user.screen_name;
			nombre_usuario = twit.user.name;
			text = twit.full_text;
			fecha = twit.created_at;
			retweeted = twit.retweet_count;
			favorite = twit.favorite_count;
			url_corta = "";

			if (twit.entities.urls[0].url){
				url_corta = twit.entities.urls[0].url;
				text = text.replace (url_corta, '');
			}

			text = text.replace(/\n/g, "<br />");

			fecha_standar = moment(fecha, "ddd MMM DD HH:mm:ss ZZ YYYY").format("YYYY/MM/DD HH:mm:ss");  
			
			moment.locale('es');
			fecha_corta = moment(fecha_standar).format("D MMM"); 
			fecha_relativa = moment(fecha_standar).fromNow(); 
			moment.locale('en');

			if (moment().diff(fecha_standar, 'days') > 1)
				fecha_relativa = "";
			else
				fecha_corta = "";
			
			$(".carga").hide();
	    	$(".container").append("<div id='"+ id_str +"' class='row border bg-white p-2'>" +
	    								
				htmlRT +
				"<div class='col-12'>" +
					"<img class='float-left mr-2 mt-2 rounded-circle imagen' src='"+ img_perfil +"' alt='"+ usuario +"'>" +

					"<div class='p-2'>" +
						"<div class='float-right'><small><i class='fas fa-clock'></i>&nbsp;"+ fecha_corta + "  " + fecha_relativa +"</small></div>"+
              			"<div><strong class='usuario'>"+ nombre_usuario +"</strong></div>"+
						"<div><small><em><a href='https://twitter.com/thepracticaldev' target='_blank' class='text-dark'>"+ usuario +"</a></em></small></div>"+
					"</div>"+	
				"</div>"+

				"<div class='row'>"+
					"<div class='col cursor' data-toggle='modal' data-target='#twtModal' idtwitter='"+ id_str +"'>" +
						"<p class='p-2 texto'>"+ text +"</p>"+
					"</div>"+
				"</div>" +
				"<span class='retweeted d-none'>"+retweeted+"</span>" +
				"<span class='favorite d-none'>"+favorite+"</span>" +
				"<span class='enlace d-none'>"+url_corta+"</span>" +
				
			"</div> <br />");
		}
	});

	$('#twtModal').on('shown.bs.modal', function (event) {
	 
		var button = $(event.relatedTarget);
	 	id = button.attr('idtwitter');
	 	
	 	texto = $("#"+id).find(".texto").html();
	 	imagen = $("#"+id).find(".imagen").attr("src");
	 	usuario = $("#"+id).find(".imagen").attr("alt");
	 	nombre_usuario = $("#"+id).find(".usuario").html();
	 	retweeted = $("#"+id).find(".retweeted").html();
		favorite = $("#"+id).find(".favorite").html();
		enlace = $("#"+id).find(".enlace").text(); 

	  	var modal = $(this)
	  	modal.find('.modal-header .imagen').attr("src", imagen);
	  	modal.find('.usuario').html(nombre_usuario);
	  	modal.find('.enlace').attr("href", enlace);
	  	modal.find('.enlace').html(enlace);
	  	modal.find('.modal-body .texto').html(texto);
	  	modal.find('.modal-footer .retweeted').text(retweeted);
	  	modal.find('.modal-footer .favorite').text(favorite);

	});
})